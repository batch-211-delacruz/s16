console.log("Hello World");


// Arithmetic Operators

	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let difference = x - y;
	console.log("Result of subtraction operator " + difference);

	let product = x * y;
	console.log("Result of multiplication operator " + product);

	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	// modulo important for divisibility
	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);

// Assignment Operators

	// Basic Assignment Operator

	let assignmentNumber = 8;

	// Addition Assignment Operator
	// Addition Assignment Operator adds the value of the right operand to a variable and assigns the result to the variable

	assignmentNumber = assignmentNumber + 2;
	console.log("Result of addition assignemnt operator: " + assignmentNumber); //result 10 reassignment of 8 adding 2

	assignmentNumber += 2;
	console.log("Result of addition assignment operator " + assignmentNumber); // gets the new assignment then adds two they are the same but it is shorthand

	//Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)

	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator " + assignmentNumber);


	// Multiple Operator and Parentheses

	/*
		-When multiple operators are applied in a single statement, it follows the PEMDAS (Parentheses, Exponents, Multiplication, Division, Addition, and Subtraction) rule
		1. 3 * 4
		2. 12 / 5
		3. 1 + 2 
		4. 3 - 2.4 = 0.6


	*/

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operation: " + mdas); //0.6

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of pemdas operation: " + pemdas);

	/*
			-By adding parentheses "()" to create more complex computations will change the order of operations still ff the same rule
			1. 4 / 5 = 0.8
			2. 2 - 3 = -1
			3. -1 * 0.8 = -0.8
			4. 1 + -0.8 = .2 round off 0.19999999...
	*/

	// Increment and Decrement
		//Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

		let z = 1;
		// The value of "z" is added by a value of one before returnng the value and storing it in the variable "increment"
		let increment = ++z;
		console.log("Result of pre-increment: " + increment);
		//The value of "z" was also increase even though we didn't implicity specify any value reassignment
		console.log("Result of pre-increment: " + z);


		// the value of "z" is returned and stores in the variable "increment" then the value of "z" is increased by one

		increment = z++;
		//the value of "z" is at 2 before it was incremented
		console.log("Result of post-increment: " + increment);
		//the value of "z" was increased again reassigning the value to 3
		console.log("Result of post-increment: " + z);


		let decrement = --z;
		console.log("Result of pre-decrement: " + decrement);
		console.log("Result of pre-decrement: " + z);


		decrement = z--;
		console.log("Result of post-decrement: " + decrement);
		console.log("Result of post-decrement: " + z);


		//Type coercion
// diff data type string and integer 12 was force to become string
		let numA = '10';
		let numB = 12; // result 1012

		/*
			-adding/concatenating a string and a number will result in a string
			-this can be proven in the console by looking at the color of the text displayed
		*/

		let coercion = numA + numB;
		console.log(coercion);
		console.log(typeof coercion);


		let numC = 16;
		let numD = 14;

		/*
		-The result is a number
		-this can be proven in the console by looking at the color of the text displayed
		-blue text means that the output returned is a number data type
		*/

		let nonCoercion = numC + numD;
		console.log(nonCoercion);
		console.log(typeof nonCoercion);

		/*
		-the result is a number
		-the boolean "true" is also associated with the value of 1
		*/


		let numE = true + 1;
		console.log(numE);

		/*
		-the result is a number
		-the boolean "false" is also associated with the value of 0
		*/

		let numF = false + 1;
		console.log(numF);

		// Comparison Operators

		let juan = 'juan';

		// Equality Operator (==)

		/*
			-checks wether the operands are equal/have the SAME content
			-attempts to convert and COMPARE operands of diifferent data types
			-returns a boolean value
		*/

		console.log(1 == 1); //true
		console.log(1 == 2); //false
		console.log(1 == '1'); //true
		console.log(0 == false); //true
		console.log('juan' == 'juan');
		console.log('juan' == juan);//true declared the value of "juan"

		//Inequality Operator

		/*
		-checks wether the operands are not equal/have DIFFERENT content
		-attempts to CONVERT and COMPARE operands of differrent data types
		*/

		console.log(1 != 1); //false
		console.log(1 != 2); //true
		console.log(1 != '1'); //false
		console.log(0 != false); //false
		console.log('juan' != 'juan'); //false
		console.log('juan' != juan);//false

		// Strict Equality Operator
		/*
		-checks wether the operands are equal/have the same content
		-also it COMPARES the data type of 2 values
		-JavaScript is a loosely types language meaning that values of different data types can be stored in variables
		-strict equality operators are better to use in most cases to ensure that data types are correct
		*/

		console.log(1 === 1); //true
		console.log(1 === 2); //false
		console.log(1 === '1'); //false
		console.log(0 === false); //false
		console.log('juan' === 'juan');//true
		console.log('juan' === juan);//true


		//Strict Inequality Operator (!==)

		/*
		-checks wether the operands are not equal/have the same content
		-also COMPARES the data types of 2 value
		*/

		console.log(1 !== 1); //false
		console.log(1 !== 2); //true
		console.log(1 !== '1'); //true
		console.log(0 !== false); //true
		console.log('juan' !== 'juan');//false
		console.log('juan' !== juan);//false


		//Relational Operators

		let a = 50;
		let b = 65;

		let isGreaterThan = a > b;
		let isLessThan = a < b;
		let isGTorEqual = a >= b;
		let isLTorEqual = a <= b;

		console.log(isGreaterThan);//false
		console.log(isLessThan);//true
		console.log(isGTorEqual);//false
		console.log(isLTorEqual);//true

		let numStr = "30";
		//coercion to change the string to a number
		console.log(a > numStr);//true
		console.log(b <= numStr);//false

		let str = "twenty";
		console.log(b >= str); // false

		//since the string is not numeric
		//the string was converted to a number
		//result to NaN. 65 is not greater than NaN

		// Logical Operator (&&, ||)
		let isLegalAge = true;
		let isRegistered = false;

		//Logical and Operator (&& - double Ampersand)
		//Return TRUE if all operands are true
		let allRequirementsMet = isLegalAge && isRegistered;
		console.log('Result of logical AND Operator: ' + allRequirementsMet);

		//Logical Or operator (|| - Double Pipe)
		//Return TRUE if one of the operands are true

		let someRequirementsMet = isLegalAge || isRegistered;
		console.log("Result of logical OR Operator: " + someRequirementsMet);
		//true


		//Logical Not Operator (! - Exclamation Point)
		//returns the opposite value

		let someRequirementsNotMet = !isRegistered;
		console.log("Result of logical NOT Operator: " + someRequirementsNotMet);

		//"typeof" operator is used to check the data type of a value/expression and returns a string value of what the data type is